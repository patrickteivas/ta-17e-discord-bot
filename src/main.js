const Discord = require("discord.js");
const client = new Discord.Client();
const commands = require("./commands");
const prefix = "/";

const express = require("express");
const http = require('http');
const cmd = require('node-cmd');

client.on("ready", () => {
    console.log(`Logged in as ${client.user.tag}!`);
    client.user.setPresence({ game: { name: "/help or /help!TarkvaraBot" } });
});

client.on("message", message => {
    if(message.author.bot) {
        return false;
    }
    const msg = message.content;
    if (!msg.startsWith(prefix) || message.author.bot) return;

    const args = msg.slice(prefix.length).split(/ +/);
    const sentCommand = args.shift().toLowerCase();

    message.content = sentCommand;

    Object.values(commands).forEach(async (command) => {
        const response = await command(message);
        if (response) {
            if (response.isPromise) {
                response.promise.then(function (value) {
                    var data = value.data;
                    var embedMsg = new Discord.RichEmbed()
                        .setTitle(data.caption)
                        .setImage(data.image);
                    message.channel.sendEmbed(embedMsg);
                })
                    .catch(function (error) {
                        console.log(error);
                    });
            } else {
                message.reply(response);
            }
        }
    });
});

client.login(process.env.BOT_TOKEN);

const app = express();
app.get('/', (request, response) => {
  console.log(`${Date.now()} Ping Received`);
  response.sendStatus(200);
});
app.listen(process.env.PORT);
setInterval(() => {
  http.get(`http://${process.env.PROJECT_DOMAIN}.glitch.me/`);
}, 270000);
setInterval(() => {
  cmd.run('refresh');
}, 3600000);

app.post('/git', (req, res) => {
  if (req.headers['x-gitlab-event'] === 'Push Hook') {
    cmd.run('chmod 777 src/git.sh');
    cmd.get('src/git.sh', (err, data) => {
      if (data) console.log(data);
      if (err) console.log(err);
    });
    cmd.run('refresh');

    console.log('> [GITLAB] Updated with gitlab/master');
  }
  return res.sendStatus(200);
});
