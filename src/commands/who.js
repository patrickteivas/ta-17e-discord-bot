module.exports = async (msg) => {
    if (msg.content !== "who" && msg.content !== "who!tarkvarabot") {
        return false;
    }
    return `Hey, I'm TarkvaraBot (${process.env.CI_COMMIT_SHORT_SHA || "xxx"}) and Patrick Teivas is my creator! https://gitlab.com/patrickteivas/ta-17e-discord-bot/tree/bot-code`;
};