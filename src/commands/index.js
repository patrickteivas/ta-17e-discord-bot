const who = require("./who");
const help = require("./help");
const meme = require("./meme");

module.exports = [
    who,
    help,
    meme,
];
