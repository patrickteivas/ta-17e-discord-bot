const meme = require("./meme");


it("Return false when command is not meme", async () => {
    var msg = {
        content: "test"
    };
    var result = await meme(msg);
    expect(result).toBeFalsy();
});

it("Return promise when command is meme", async () => {
    var msg = {
        content: "meme"
    };

    var result = await meme(msg);
    expect(result.isPromise).toBeTruthy();
    
    result.promise.then(function (value) {
        var data = value.data;
        expect(data.caption).toBeTruthy();
        expect(data.image).toBeTruthy();
    });
});