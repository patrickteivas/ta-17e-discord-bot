module.exports = async (msg) => {
    if (msg.content !== "help" && msg.content !== "help!tarkvarabot") {
        return false;
    }
    return `I have following commands available:
 * /help / /help!TarkvaraBot - show commands help
 * /who / /who!TarkvaraBot - who is my creator
 * /meme - get random meme`;
};