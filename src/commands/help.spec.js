const help = require("./help");


it("Return false when command is not help or help!tarkvarabot", async () => {
    var msg = {
        content: "test"
    };
    var result = await help(msg);
    expect(result).toBeFalsy();
});

it("Return true when command is help", async () => {
    var msg = {
        content: "help"
    };
    var result = await help(msg);
    expect(result).toBeTruthy();
});

it("Return true when command is help!tarkvarabot", async () => {
    var msg = {
        content: "help!tarkvarabot"
    };
    var result = await help(msg);
    expect(result).toBeTruthy();
});