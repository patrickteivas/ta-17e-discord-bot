const who = require("./who");


it("Return false when command is not who or who!tarkvarabot", async () => {
    var msg = {
        content: "test"
    };
    var result = await who(msg);
    expect(result).toBeFalsy();
});

it("Return true when command is who", async () => {
    var msg = {
        content: "who"
    };
    var result = await who(msg);
    expect(result).toBeTruthy();
});

it("Return true when command is who!tarkvarabot", async () => {
    var msg = {
        content: "who!tarkvarabot"
    };
    var result = await who(msg);
    expect(result).toBeTruthy();
});